#!/usr/bin/env python3
import numpy as np
import cv2
#import imutils
import tensorflow as tf
import tensorflow.keras as keras
import time

#load pretrained keras model
model = keras.models.load_model("model/model.h5") 

#capture video from webcam
cap = cv2.VideoCapture(0)

#background image to substract from current frame
background = None

#time between frames
started = False
curr = time.time()
prev = curr

#number of frames per second
fps = 24

#debug flag
debug = True

while True:
  curr = time.time()
  if curr - prev > 1 / fps:
    ret, frame = cap.read()
    frame_copy = frame.copy()
    if started:
      prev = time.time()
      gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
      #substract background 
      mask = background - gray.astype(np.int16)
      mask[mask < 0] = 0
      mask = mask.astype(np.uint8)
      mask = cv2.medianBlur(mask, 5)

      mask = cv2.threshold(mask, 12, 255, cv2.THRESH_BINARY)[1]

      cnts, _ = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

      if (len(cnts) > 0):
        #find contour with largest area
        c = max(cnts, key=cv2.contourArea)

        extLeft = tuple(c[c[:, :, 0].argmin()][0])
        extRight = tuple(c[c[:, :, 0].argmax()][0])
        extTop = tuple(c[c[:, :, 1].argmin()][0])
        extBot = tuple(c[c[:, :, 1].argmax()][0])

        x, y = extLeft[0] - 20, extTop[1] - 20
        a = extRight[0] - extLeft[0] + 40
        cv2.rectangle(frame, (x, y), (x + a, y + a), (0,255,0), 2)

        #with background
        #cropped = gray[y:y+a, x:x+a]

        #without background
        tmp = np.zeros_like(gray) + 255
        contour_mask = np.zeros_like(gray) + 255
        cv2.drawContours(contour_mask, [c], -1, 0, -1)
        tmp[contour_mask == 0] = gray[contour_mask == 0]
        cropped = tmp[y:y+a, x:x+a]

        if debug:
          #x, y, w, h = cv2.boundingRect(c)
          #cv2.rectangle(frame, (x, y), (x + w, y + h), (0,255,0), 2)
          cv2.drawContours(frame, [c], -1, (0, 255, 255), 2)
          cv2.circle(frame, extLeft, 8, (0, 0, 255), -1)
          cv2.circle(frame, extRight, 8, (0, 255, 0), -1)
          cv2.circle(frame, extTop, 8, (255, 0, 0), -1)
          cv2.circle(frame, extBot, 8, (255, 255, 0), -1)
          
          if (cropped.shape[0] > 28 and cropped.shape[1] > 28):
            cv2.imshow("Cropped", tmp[y:y+a, x:x+a])

        if cropped.shape[0] > 28 and cropped.shape[1] > 28:
          image = cv2.resize(cropped, (28, 28))
          image = image.astype(np.float32) / 255
          if (debug):
            cv2.imshow('Model input', image)
          image = np.expand_dims(image, axis = 0)
          image = np.expand_dims(image, axis = 3)
          pred = model.predict_classes(image)
          letter = chr(ord("A") + pred)

          cv2.putText(frame,letter, (x,y), cv2.FONT_HERSHEY_SIMPLEX, 
              1, (255,255,255), 2)

      out = mask
    else:
      out = frame
    
    # Display the resulting frame
    cv2.imshow('frame', frame)
    cv2.imshow('output', out)
    
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
      break
    elif key == ord('s'):
      #save background and convert to 16bit integer dtype to prevent uint8 overflow
      background = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY).astype(np.int16)
      started = True
    elif key == ord('p'):
      started = False

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
