# Description 
Sign language classification using Keras.

# Files 

* `Porokhnia_Projekt.ipynb` - model training, dataset description 
* `detection.py` - using pretrained model with OpenCV to classify images from webcam
